# redict

Pythonic interface to redis.

## Installation

From your favourite python virtual environment:

```$ pip install redict```

## Usage

```python
>>> import redis
>>> import redict

>>> conn = redis.Redis()
>>> rdict = redict.Dict(conn)

>>> # access rdict as an object
>>> rdict['foo'].value
b'bar'

>>> # hash values as python dictionaries as well
>>> guido = rdict['guido']
>>> guido['name']
b'Guido'

>>> # set will actually set the value in the redis db
>>> guido['email'] = 'guido@example.com'
>>> rdict['guido']['email']
b'guido@example.com'

```

## Credits

* [redis-py](https://github.com/andymccurdy/redis-py)
* [hiredis-py](https://github.com/redis/hiredis-py)
* [treelib](https://github.com/caesar0301/treelib)