"""
Internal module for common tools
"""

RAW_GET_TYPE_MAP = {
    b'none': lambda r, v: None,
    b'string': lambda r, k: r.get(k),
    b'hash': lambda r, k: r.hgetall(k),
    b'list': lambda r, k: r.lrange(k, 0, -1),
    b'set': lambda r, k: r.smembers(k)
}
