import uuid
import collections.abc


class Hash(collections.abc.MutableMapping):

    __slots__ = ['redis', 'key']

    def __init__(self, redis, key):
        self.redis = redis
        self.key = key

    def __getitem__(self, field):
        if isinstance(field, tuple):
            return self.redis.hmget(self.key, field)
        if isinstance(field, slice):
            assert field.start is None
            assert field.stop is None
            assert field.step is None
            return self.redis.hgetall(self.key)
        value = self.redis.hget(self.key, field)
        if value is None:
            raise KeyError(field)
        return value

    def __setitem__(self, field, value):
        if isinstance(field, tuple):
            self.redis.hmset(self.key, dict(zip(field, value)))
        else:
            self.redis.hset(self.key, field, value)

    def __delitem__(self, field):
        if isinstance(field, tuple):
            return self.redis.hdel(self.key, *field)
        if isinstance(field, slice):
            assert field.start is None
            assert field.stop is None
            assert field.step is None
            return self.clear()
        if not self.redis.hdel(self.key, field):
            raise KeyError(field)

    def __iter__(self):
        return iter(self.redis.hkeys(self.key))

    def __len__(self):
        return self.redis.hlen(self.key)

    def __contains__(self, field):
        return self.redis.hexists(self.key, field)

    def __eq__(self, other):
        if self is other:
            return True
        if isinstance(other, Hash):
            if self.key == other.key and self.redis == self.redis:
                return True
        return False

    def __repr__(self):
        return '{}<redis={}, key={}>'.format(type(self).__name__,
                                             self.redis, self.key)

    def __str__(self):
        return str(dict(self))

    def values(self):
        return self.redis.hvals(self.key)

    def items(self):
        return self[:].items()

    def get(self, field, default=None):
        value = self.redis.hget(self.key, field)
        return default if value is None else value

    def update(self, other):
        self.redis.hmset(self.key, other)

    def copy(self):
        return type(self)(self.redis, self.key)

    def pop(self, *args):
        field = args[0]
        result = self.redis.hdel(self.key, field)
        if result is None:
            if len(args) < 2:
                raise KeyError(field)
            result = args[1]
        return result

    def setdefault(self, field, default=None):
        if default is not None:
            r = self.redis.hsetnx(self.key, field, default)
            return default if r else self[field]
        elif field in self:
            return self[field]
        return None

    def clear(self):
        return self.redis.delete(self.key)

    @property
    def value(self):
        return self[:]

    @value.setter
    def value(self, value):
        self.clear()
        self.update(value)


class List(collections.abc.MutableSequence):

    __slots__ = ['redis', 'key']

    def __init__(self, redis, key):
        self.redis = redis
        self.key = key

    def __getitem__(self, index):
        if isinstance(index, slice):
            start, stop, step = index.start or 0, index.stop, index.step
            if stop is None:
                stop = -1
            value = self.redis.lrange(self.key, start, stop)
            if step not in (None, 1):
                value = value[::step]
        else:
            value = self.redis.lindex(self.key, index)
        if value is None:
            raise IndexError('redis list index out of range')
        return value

    def __setitem__(self, index, value):
        if isinstance(index, slice):
            if index.start is not None or index.stop is not None and index.step is not None:
                old_value = self[:]
                value = old_value[index] = value
            self.clear()
            self += value
        else:
            self.redis.lset(self.key, index, value)

    def __delitem__(self, index):
        if isinstance(index, slice):
            if index.start is None and index.stop is None and index.step is None:
                self.clear()
            else:
                value = self[:]
                del value[index]
                self[:] = value
        value = uuid.uuid1()
        self.redis.lset(self.key, index, value)
        self.redis.lrem(self.key, 1, value)

    def __len__(self):
        return self.redis.llen(self.key)

    def __iadd__(self, value):
        self.redis.rpush(self.key, *value)
        return self

    def __eq__(self, other):
        if self is other:
            return True
        if isinstance(other, List):
            if self.key == other.key and self.redis == self.redis:
                return True
        return False

    def __repr__(self):
        return '{}<redis={}, key={}>'.format(type(self).__name__,
                                             self.redis, self.key)

    def __str__(self):
        return str(self[:])

    def insert(self, index, value):
        if index == 0:
            return self.redis.lpush(value)
        raise NotImplementedError

    def pop(self, index=-1):
        if index == -1:
            return self.redis.rpop(self.key)
        elif index == 0:
            return self.redis.lpop(self.key)
        raise NotImplementedError

    def pop_into(self, dst, index=-1, dst_index=0):
        dst_key = dst.key if isinstance(dst, List) else dst
        if index == -1:
            if dst_index == 0:
                return self.redis.rpoplpush(self.key, dst_key)
        raise NotImplementedError

    def append(self, value):
        self.redis.rpush(self.key, value)

    def copy(self):
        return type(self)(self.redis, self.key)

    def clear(self):
        return self.redis.delete(self.key)

    @property
    def value(self):
        return self[:]

    @value.setter
    def value(self, value):
        self[:] = value


class Set(collections.abc.MutableSet):

    __slots__ = ['redis', 'key']

    def __init__(self, redis, key):
        self.redis = redis
        self.key = key

    def __contains__(self, member):
        return self.redis.sismember(self.key, member)

    def __iter__(self):
        return self.redis.sscan_iter(self.key)

    def __len__(self):
        return self.redis.scard(self.key)

    def __eq__(self, other):
        if self is other:
            return True
        if isinstance(other, Set):
            if self.key == other.key and self.redis == self.redis:
                return True
        return False

    def __add__(self, other):
        if isinstance(other, Set):
            assert self.redis == other.redis
            self.redis.sunionstore(self.key, self.key, other.key)
        else:
            self.redis.sadd(self.key, *tuple(other))

    def __sub__(self, other):
        if isinstance(other, Set):
            assert self.redis == other.redis
            self.redis.sdiffstore(self.key, self.key, other.key)
        else:
            self.redis.srem(self.key, *tuple(other))

    def add(self, item):
        self.redis.sadd(self.key, item)

    def discard(self, item):
        self.redis.srem(self.key, item)

    def clear(self):
        return self.redis.delete(self.key)

    @property
    def value(self):
        return set(self)

    @value.setter
    def value(self, value):
        self[:] = value


class Value:

    __slots__ = ['redis', 'key']

    def __init__(self, redis, key):
        self.redis = redis
        self.key = key

    def __len__(self):
        return self.redis.strlen(self.key)

    def __int__(self):
        return int(self.value)

    def __float__(self):
        return float(self.value)

    def __iadd__(self, value):
        if isinstance(value, int):
            self.redis.incrby(self.key, value)
        elif isinstance(value, (str, bytes)):
            self.redis.append(self.key, value)
        else:
            self.redis.incrbyfloat(self.key, value)
        return self

    def __isub__(self, value):
        if isinstance(value, int):
            self.redis.incrby(self.key, -value)
        else:
            self.redis.incrbyfloat(self.key, -value)
        return self

    def __eq__(self, other):
        if self is other:
            return True
        if isinstance(other, Value):
            if self.key == other.key and self.redis == self.redis:
                return True
        return False

    def __repr__(self):
        return '{}<redis={}, key={}>'.format(type(self).__name__,
                                             self.redis, self.key)

    def __str__(self):
        return str(self.value)

    @property
    def value(self):
        return self.redis.get(self.key)

    @value.setter
    def value(self, value):
        self.redis.set(self.key, value)


GET_TYPE_MAP = {
    b'none': lambda r, v: None,
    b'string': Value,
    b'hash': Hash,
    b'list': List,
    b'set': Set,
}


def get_redis_type(value):
    if isinstance(value, (str, bytes, int, float, Value)):
        return b'string'
    elif isinstance(value, (collections.abc.Sequence, List)):
        return b'list'
    elif isinstance(value, (collections.abc.Mapping, Hash)):
        return b'hash'
    elif isinstance(value, (collections.abc.Set, Set)):
        return b'set'


def get_redict_type(value):
    return GET_TYPE_MAP[get_redis_type(value)]


def _unique_keys(keys, separator, prefix=''):
    if prefix:
        prefix_size = len(prefix)
        keys = (key[prefix_size:] for key in keys)
    keys = (key.split(separator, 1)[0] for key in keys)
    seen = set()
    for key in keys:
        if key in seen:
            continue
        seen.add(key)
        yield key


class Dict(collections.abc.MutableMapping):
    """Python dict protocol over a Redis database"""

    def __init__(self, redis, key=None, separator=None):
        assert redis is not None
        assert key is None or len(key)
        assert separator is None or len(separator)
        self.redis = redis
        self.separator = separator
        self.key = key

    def _full_key(self, key):
        if self.separator and self.key:
            return self.key + self.separator + key
        return key

    @property
    def rvalue(self):
        """Redict value object (Value, List, Hash, Set)"""
        if self.key is None:
            raise ValueError('Redis Dict does not have a value')
        dtype = self.redis.type(self.key)
        if dtype == b'none':
            return None
        func = GET_TYPE_MAP[dtype]
        return func(self.redis, self.key)

    @property
    def value(self):
        """Redis value in the database for this object key"""
        vobj = self.rvalue
        return vobj if vobj is None else vobj.value

    def __getitem__(self, key):
        if isinstance(key, tuple):
            return [self[k] for k in key]
        if self.separator is None:
            # if we have a flat dict then the key must exist
            if not self.redis.exists(key):
                raise KeyError(key)
        result = Dict(self.redis, key=self._full_key(key),
                      separator=self.separator)
        return result

    def __setitem__(self, key, value):
        Obj = get_redict_type(value)
        obj = Obj(self.redis, self._full_key(key))
        obj.value = value

    def __delitem__(self, key):
        keys = keys if isinstance(key, tuple) else key,
        self.redis.delete(*(self._full_key(k) for k in keys))

    def __iter__(self):
        if self.key is None:
            gen = self.redis.scan_iter()
            if self.separator is not None:
                gen = _unique_keys(gen, self.separator)
        else:
            prefix = self.key + self.separator
            gen = self.redis.scan_iter(match=prefix + b'*')
            gen = _unique_keys(gen, self.separator, prefix)
        return gen

    def __len__(self):
        if self.key is None and self.separator is None:
            return self.redis.dbsize()
        #TODO find a better way to get the size (probably with a redis.eval)
        return len(tuple(iter(self)))

    def __contains__(self, field):
        return bool(self.redis.exists(field))

    def __eq__(self, other):
        if self is other:
            return True
        if isinstance(other, Dict):
            if self.key == other.key and self.redis == self.redis:
                return True
        return False

    def __repr__(self):
        return repr(self.redis)

    def values(self):
        raise NotImplementedError

    def items(self):
        raise NotImplementedError

    def clear(self):
        if self.key is None and self.separator is None:
            return self.redis.flushdb()
        #TODO find a better way to delete keys (probably with a redis.eval)
        self.redis.delete(*self)

    def list(self, key, iterable=None):
        '''create a list'''
        lst = List(self.redis, key)
        if iterable is not None:
            self.redis.delete(key)
            self.redis.rpush(key, *tuple(iterable))
        return lst

    def dict(self, key, *args, **kwargs):
        hsh = Hash(self.redis, key)
        if args or kwargs:
            hsh.update(dict(*args, **kwargs))
        return hsh

    def set(self, key, *args, **kwargs):
        raise NotImplementedError


