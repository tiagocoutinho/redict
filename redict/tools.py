from .common import RAW_GET_TYPE_MAP


def count(redis, match=None):
    """count number of keys with a given pattern"""
    if match is None or match == '*':
        return redis.dbsize()
    return redis.eval("return #redis.pcall('keys', ARGV[1])", 0, match)


def iter_scan(redis, match=None, count=None):
    """functional version of redis.scan_iter()"""
    for key in redis.scan_iter(match=match, count=count):
        yield key


def get(redis, key):
    dtype = redis.type(key)
    func = RAW_GET_TYPE_MAP[dtype]
    return func(redis, key)


class iter_keys:
    """
    Like iter_scan() but with with the ability to tell the expected length
    """

    def __init__(self, redis, match='*'):
        self._count = count(redis, match)
        self._gen = iter_scan(redis, match=match, count=self._count)

    def __len__(self):
        return self._count

    def __iter__(self):
        return self

    def __next__(self):
        return next(self._gen)


def iter_items(redis, match=None, count=None):
    for key in iter_scan(redis, match, count):
        yield key, get(redis, key)


class iter_items_progress:
    """
    Like iter_items() but the generator yields a tuple of 2 elements:
    * progress: (current, total)
    * item: (key, value)

    Also it is able to tell you its expected length.
    """

    def __init__(self, redis, match=None):
        self._count = count(redis, match)
        self._gen = self._build(redis, match)

    def _build(self, redis, match):
        for i, item in enumerate(iter_items(redis, match, self._count)):
            yield (i, self.n), item

    def __len__(self):
        return self._count

    def __iter__(self):
        return self

    def __next__(self):
        return next(self._gen)


def dump(redis, match=None, count=None):
    return dict(iter_items(redis, match, count))
