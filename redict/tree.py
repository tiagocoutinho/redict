import treelib

from .core import Dict
from .tools import iter_keys


class Tree(treelib.Tree):
    """
    A snapshot tree based view of the redis database keys.

    The tree structure is calculated at creation time. It may be updated at anytime
    by calling reload().

    The data of each node is a Dict, List, Value or Set so getting the value will always
    get the current DB value.
    """

    def __init__(self, redis, separator=':', pattern='*', with_data=True):
        super().__init__()
        self.redis = redis
        self._dic = Dict(redis)
        self._separator = separator
        self._pattern = pattern
        self._with_data = with_data
        self.reload()

    def reload(self):
        if len(self):
            self.remove_node(self.root)
            self.root = None
        root = self.create_node(tag=repr(self.redis), identifier=0, data=self._dic)
        separator = self._separator
        for key in iter_keys(self.redis, match=self._pattern):
            ukey = key.decode()
            parent = root
            for part in ukey.split(separator):
                if parent is root:
                    this = part
                else:
                    this = parent.identifier + separator + part
                node = self.get_node(this)
                if node is None:
                    node = self.create_node(tag=part, identifier=this,
                                            parent=parent)
                parent = node
            if self._with_data:
                node.data = self._dic[key]


