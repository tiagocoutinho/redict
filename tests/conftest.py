import fnmatch

import pytest

import redict


class Redis:

    def __init__(self, data=None):
        self._data = {} if data is None else data

    def keys(self, pattern='*'):
        pattern = pattern.encode() if isinstance(pattern, bytes) else pattern
        return fnmatch.filter(self._data.keys(), pattern.encode())

    def get(self, key):
        return self._data.get(key)

    def set(self, key, value, nx=False, xx=False):
        exists = key in self._data
        if (nx and not exists) or (xx and exists) or (not nx and not xx):
            self._data[key] = value

    def dbsize(self):
        return len(self._data)

    def scan_iter(self, match=None, count=None):
        match = '*' if match is None else match
        for key in self.keys(match):
            yield key

    def exists(self, key):
        return key in self._data

    def type(self, key):
        v = self._data.get(key)
        if v is None:
            return b'none'
        elif isinstance(v, (tuple, list)):
            return b'list'
        elif isinstance(v, dict):
            return b'hash'
        elif isinstance(v, set):
            return b'set'
        return b'string'

    def lrange(self, key, start, end):
        end = None if end == -1 else end + 1
        return self._data[key][start:end]

    def incrby(self, key, value):
        new_value = int(self._data[key]) + value
        new_value_bytes = str(new_value).encode()
        self._data[key] = new_value_bytes
        return new_value_bytes


@pytest.fixture
def rdis():
    dis = Redis({b'foo': b'bar',
                 b'i1': b'56',
                 b'f1': b'1234.5678',
                 b'l1': [b'1', b'2', b'3'],
                 b'd1': {b'a': 1, b'b': 2, b'c': 'foo'}})
    return dis


@pytest.fixture
def flat_dict(rdis):
    return redict.Dict(rdis)
