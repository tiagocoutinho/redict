import pytest

import redict


def test_flat_dict_len(rdis):
    d = redict.Dict(rdis)
    assert len(d) == 5
    rdis.set(b'hello', b'world')
    assert len(d) == 6


def test_flat_dict_root(rdis):
    d = redict.Dict(rdis)
    with pytest.raises(ValueError):
        d.value
    with pytest.raises(ValueError):
        d.rvalue


def test_flat_dict_keys(rdis):
    initial = {b'foo', b'l1', b'd1', b'i1', b'f1'}
    d = redict.Dict(rdis)
    assert set(d.keys()) == initial
    rdis.set(b'hello', b'world')
    new = set(initial)
    new.add(b'hello')
    assert set(d.keys()) == new


def test_flat_dict_getitem(flat_dict):
    v1_a = flat_dict[b'foo']
    v1_b = flat_dict[b'foo']
    assert isinstance(v1_a, redict.Dict)
    assert isinstance(v1_b, redict.Dict)
    assert isinstance(v1_a.rvalue, redict.Value)
    assert isinstance(v1_b.rvalue, redict.Value)
    assert v1_a.value == b'bar'
    assert v1_b.value == b'bar'
    assert v1_a == v1_b

    with pytest.raises(KeyError):
        flat_dict[b'non existing']

    l1 = flat_dict[b'l1']
    assert isinstance(l1, redict.Dict)
    assert isinstance(l1.rvalue, redict.List)
    assert l1.value == [b'1', b'2', b'3']


def test_flat_dict_equals(flat_dict):
    flat_dict2 = redict.Dict(flat_dict.redis)

    assert flat_dict == flat_dict2


def test_value(flat_dict):
    rdis = flat_dict.redis

    foo1 = flat_dict[b'foo']
    foo2 = redict.Value(rdis, b'foo')

    assert foo1.value == b'bar'
    assert foo2.value == b'bar'
    assert foo1.rvalue == foo2
    assert foo1.value == foo2.value
    assert not foo1 == foo2
    assert not foo1.rvalue == foo2.value

    rdis.set(b'foo', b'beer')

    assert foo1.value == b'beer'
    assert foo2.value == b'beer'
    assert foo1.rvalue == foo2
    assert foo1.value == foo2.value
    assert not foo1 == foo2
    assert not foo1.rvalue == foo2.value


def test_value_int(flat_dict):
    rdis = flat_dict.redis
    i1_a = flat_dict[b'i1']
    i1_b = redict.Value(rdis, b'i1')

    assert i1_a.value == b'56'

    i1_b += 45

    assert rdis.get(b'i1') == b'101'
    assert i1_a.value == b'101'
    assert i1_b.value == b'101'

    i1_ar = i1_a.rvalue
    i1_ar -= 12

    assert i1_a.value == b'89'
    assert i1_b.value == b'89'
